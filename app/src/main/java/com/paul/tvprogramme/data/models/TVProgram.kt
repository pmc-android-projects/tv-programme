package com.paul.tvprogramme.data.models

/**
 * Created by Paul on 7/29/2015.
 */
class TVProgram {

    var name: String? = null
    var start_time: String? = null
    var end_time: String? = null
    var channel: String? = null
    var rating: String? = null

    override fun toString(): String {
        return name!!
    }
}
