package com.paul.tvprogramme.data.models

/**
 * Created by pcastillo on 05/09/2016.
 */
class GuideResponse {
    var results: MutableList<TVProgram>? = null
    var count: Int = 0
}
