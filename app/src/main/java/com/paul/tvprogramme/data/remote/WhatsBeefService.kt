package com.paul.tvprogramme.data.remote

import com.paul.tvprogramme.data.models.GuideResponse
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

/**
 * Created by pcastillo on 05/09/2016.
 */
interface WhatsBeefService {

    @GET("wabz/guide.php")
    fun getGuides(@Query("start") start: Int): Observable<GuideResponse>
}
