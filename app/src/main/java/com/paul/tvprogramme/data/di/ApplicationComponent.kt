package com.paul.tvprogramme.data.di

import dagger.Component
import retrofit2.Retrofit

import javax.inject.Singleton

/**
 * Created by pcastillo on 11/10/2016.
 */

@Singleton
@Component(modules = arrayOf(RetrofitModule::class))
interface ApplicationComponent {
    fun retrofit(): Retrofit
}
