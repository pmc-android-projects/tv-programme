package com.paul.tvprogramme.features.main

import com.paul.tvprogramme.utils.CustomScope

import dagger.Module
import dagger.Provides

/**
 * Created by pcastillo on 11/10/2016.
 */

@Module
class MainModule(private val mView: MainContract.View) {

    @Provides
    @CustomScope
    fun provideMainContractView(): MainContract.View {
        return mView
    }
}
