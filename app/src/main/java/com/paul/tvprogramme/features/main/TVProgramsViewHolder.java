package com.paul.tvprogramme.features.main;

import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.paul.tvprogramme.R;
import com.paul.tvprogramme.data.models.TVProgram;
import com.vstechlab.easyfonts.EasyFonts;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pcastillo on 06/09/2016.
 */
public class TVProgramsViewHolder extends BaseViewHolder<TVProgram> {

    @BindView(R.id.list_primary_text)
    public TextView tvName;
    @BindView(R.id.list_secondary_text)
    public TextView tvStartTime;
    @BindView(R.id.list_tertiary_text)
    public TextView tvEndTime;
//    @BindView(R.id.tvChannel)
//    public TextView tvChannel;
//    @BindView(R.id.tvRating)
//    public TextView tvRating;

    public TVProgramsViewHolder(ViewGroup parent) {
        super(parent, R.layout.md_list_three_line);
        ButterKnife.bind(this, itemView);

        tvName.setTypeface(EasyFonts.robotoRegular(getContext()));
        tvStartTime.setTypeface(EasyFonts.robotoRegular(getContext()));
        tvEndTime.setTypeface(EasyFonts.robotoRegular(getContext()));
//        tvChannel.setTypeface(EasyFonts.robotoRegular(getContext()));
//        tvRating.setTypeface(EasyFonts.robotoRegular(getContext()));
    }

    @Override
    public void setData(TVProgram data) {
        tvName.setText(data.getName());
        tvStartTime.setText(getContext().getString(R.string.label_time_start, data.getStart_time()));
        tvEndTime.setText(getContext().getString(R.string.label_time_end, data.getEnd_time()));
//        tvChannel.setText(getContext().getString(R.string.label_channel, data.getChannel()));
//        tvRating.setText(getContext().getString(R.string.label_rating, data.getRating()));
    }
}
