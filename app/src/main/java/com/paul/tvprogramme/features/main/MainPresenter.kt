package com.paul.tvprogramme.features.main

import com.paul.tvprogramme.data.models.GuideResponse
import com.paul.tvprogramme.data.models.TVProgram
import com.paul.tvprogramme.data.remote.WhatsBeefService

import java.util.ArrayList

import javax.inject.Inject

import retrofit2.Retrofit
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import timber.log.Timber

/**
 * Created by pcastillo on 11/10/2016.
 */
class MainPresenter @Inject
constructor(private val mView: MainContract.View) : MainContract.Presenter {
    private var tvPrograms: MutableList<TVProgram>? = ArrayList()

    @Inject
    var retrofit: Retrofit? = null

    override fun getGuidesNew() {
        getGuides(0)
    }

    override fun getGuidesMore() {
        getGuides(tvPrograms!!.size)
    }

    fun getGuides(offset: Int) {
        val whatsBeefService = retrofit!!.create(WhatsBeefService::class.java)

        val getGuidesObservable = whatsBeefService.getGuides(offset)
                .repeatWhen { observable -> observable }
                .takeUntil { guideResponse ->
                    if (guideResponse != null) {
                        true
                    } else {
                        false
                    }
                }

        getGuidesObservable
                .subscribeOn(Schedulers.io()) // Create a new Thread
                .observeOn(AndroidSchedulers.mainThread()) // Use the UI thread
                .subscribe(object : Subscriber<GuideResponse>() {
                    override fun onCompleted() {
                        Timber.d("onCompleted")
                    }

                    override fun onError(e: Throwable) {
                        Timber.d("onError: " + e.message)
                        mView.showLoadingError(e.message!!)
                    }

                    override fun onNext(guideResponse: GuideResponse) {
                        Timber.d("onNext")
                        Timber.d("There where " + tvPrograms!!.size + " program(s) found.")

                        val results: MutableList<TVProgram>? = guideResponse.results
                        if (offset == 0) {
                            if (results != null) {
                                tvPrograms = results
                            }
                            mView.showList(results!!)
                        } else {
                            if (results != null) {
                                tvPrograms!!.addAll(results)
                            }
                            mView.addList(results!!)
                        }
                    }
                })
    }
}
