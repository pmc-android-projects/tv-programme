package com.paul.tvprogramme.features.main

import com.paul.tvprogramme.data.di.ApplicationComponent
import com.paul.tvprogramme.utils.CustomScope
import dagger.Component

/**
 * Created by pcastillo on 11/10/2016.
 */

@CustomScope
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(MainModule::class))
interface MainComponent {
    fun inject(mainActivity: MainActivity)
}
