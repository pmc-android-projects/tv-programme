package com.paul.tvprogramme.features.main;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.paul.tvprogramme.R;
import com.paul.tvprogramme.TVApplication;
import com.paul.tvprogramme.data.models.TVProgram;
import com.thefinestartist.converters.Unit;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Paul Mark D. Castillo
 */
public class MainActivity extends AppCompatActivity
        implements
        MainContract.View,
        RecyclerArrayAdapter.OnLoadMoreListener,
        SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.tvProgramsRecyclerView)
    EasyRecyclerView tvProgramsRecyclerView;

    private TVProgramsAdapter adapter;

    @Inject
    MainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        DaggerMainComponent.builder()
                .applicationComponent(TVApplication.Companion.getInstance().component())
                .mainModule(new MainModule(this))
                .build()
                .inject(this);

        adapter = new TVProgramsAdapter(this);
        adapter.setMore(R.layout.view_more, this);
        adapter.setNoMore(R.layout.view_nomore);
        adapter.setOnItemLongClickListener(new RecyclerArrayAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(int position) {
                adapter.remove(position);
                return true;
            }
        });
        adapter.setError(R.layout.view_error)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        adapter.resumeMore();
                    }
                });

        HorizontalDividerItemDecoration horizontalDividerItemDecoration = new HorizontalDividerItemDecoration.Builder(this)
                .color(ContextCompat.getColor(this, R.color.divider))
                .size(Unit.dpToPx(1))
                .build();

        LinearLayoutManager llm = new LinearLayoutManager(this);
        tvProgramsRecyclerView.DEBUG = true;
        tvProgramsRecyclerView.setLayoutManager(llm);
        tvProgramsRecyclerView.setAdapter(adapter);
        tvProgramsRecyclerView.setRefreshListener(this);
        tvProgramsRecyclerView.setErrorView(R.layout.view_error);
        tvProgramsRecyclerView.addItemDecoration(horizontalDividerItemDecoration);
        tvProgramsRecyclerView.showProgress();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getGuidesNew();
    }

    @Override
    public void showLoadingError(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
        tvProgramsRecyclerView.showError();
    }

    @Override
    public void onLoadMore() {
        mPresenter.getGuidesMore();
    }

    @Override
    public void onRefresh() {
        onResume();
    }

    @Override
    public void showList(@NotNull List<TVProgram> tvPrograms) {
        if (tvPrograms != null && tvPrograms.size() > 0) {
            adapter.clear();
            adapter.addAll(tvPrograms);
            tvProgramsRecyclerView.showRecycler();
        } else {
            tvProgramsRecyclerView.showEmpty();
        }
    }

    @Override
    public void addList(@NotNull List<TVProgram> tvPrograms) {
        adapter.addAll(tvPrograms);
    }
}
