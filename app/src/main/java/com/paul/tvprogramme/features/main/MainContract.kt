package com.paul.tvprogramme.features.main

import com.paul.tvprogramme.data.models.TVProgram

/**
 * Created by pcastillo on 11/10/2016.
 */
class MainContract {

    interface View {

        fun showLoadingError(message: String)

        fun showList(tvPrograms: List<TVProgram>)

        fun addList(tvPrograms: List<TVProgram>)
    }

    internal interface Presenter {

        fun getGuidesNew()

        fun getGuidesMore()
    }
}
