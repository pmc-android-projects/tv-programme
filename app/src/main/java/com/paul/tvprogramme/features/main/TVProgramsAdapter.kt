package com.paul.tvprogramme.features.main

import android.content.Context
import android.view.ViewGroup
import com.jude.easyrecyclerview.adapter.BaseViewHolder
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter
import com.paul.tvprogramme.data.models.TVProgram

/**
 * Created by Paul Mark Castillo on 9/5/2016.
 */
class TVProgramsAdapter(context: Context) : RecyclerArrayAdapter<TVProgram>(context) {

    override fun OnCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return TVProgramsViewHolder(parent)
    }
}
