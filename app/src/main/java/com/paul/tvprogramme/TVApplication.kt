package com.paul.tvprogramme

import android.content.Context
import android.support.multidex.BuildConfig
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication

import com.paul.tvprogramme.data.di.ApplicationComponent
import com.paul.tvprogramme.data.di.DaggerApplicationComponent
import com.paul.tvprogramme.data.di.RetrofitModule
import com.thefinestartist.Base

import timber.log.Timber

/**
 * Created by pcastillo on 07/09/2016.
 */
class TVApplication : MultiDexApplication() {
    private var component: ApplicationComponent? = null

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        if (instance == null) {
            instance = this
            initialize()
        }
    }

    private fun initialize() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Base.initialize(this)

        component = DaggerApplicationComponent.builder().retrofitModule(RetrofitModule).build()
    }

    fun component(): ApplicationComponent? {
        return component
    }

    companion object {
        var instance: TVApplication? = null
            private set
    }
}
