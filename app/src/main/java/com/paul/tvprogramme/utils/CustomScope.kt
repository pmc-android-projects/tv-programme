package com.paul.tvprogramme.utils

import javax.inject.Scope

/**
 * Created by rmanacmol on 7/31/2016.
 */

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class CustomScope
